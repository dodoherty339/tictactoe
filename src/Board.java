public class Board {
    private static final int ROWSIZE = 3;
    private static final int COLSIZE = 3;

    Cell[][] cellArr;

    private int cellsAvailable;

    private String winner;

    public Board() {
        cellsAvailable = 9;
        winner = null;
        cellArr = new Cell[ROWSIZE][COLSIZE];
        for (int row = 0; row < ROWSIZE; ++row) {
            for (int col = 0; col < COLSIZE; ++col) {
                cellArr[row][col] = new Cell();
            }
        }
    }

    public String getWinner() {
        return winner;
    }

    public boolean isCellAvailable(int row, int col) {
        return (cellArr[row][col].getToken() == null);
    }

    public void setBoardCell(int row, int col, String token) {
        cellsAvailable = cellsAvailable - 1;
        cellArr[row][col].setToken(token);
        checkForWinner();
    }

    private String checkForWinner() {
        String row1 = cellArr[0][0].getToken() + cellArr[0][1].getToken() + cellArr[0][2].getToken();
        String row2 = cellArr[1][0].getToken() + cellArr[1][1].getToken() + cellArr[1][2].getToken();
        String row3 = cellArr[2][0].getToken() + cellArr[2][1].getToken() + cellArr[2][2].getToken();

        String col1 = cellArr[0][0].getToken() + cellArr[1][0].getToken() + cellArr[2][0].getToken();
        String col2 = cellArr[0][1].getToken() + cellArr[1][1].getToken() + cellArr[2][1].getToken();
        String col3 = cellArr[0][2].getToken() + cellArr[1][2].getToken() + cellArr[2][2].getToken();

        String diag1 = cellArr[0][0].getToken() + cellArr[1][1].getToken() + cellArr[2][2].getToken();
        String diag2 = cellArr[0][2].getToken() + cellArr[1][1].getToken() + cellArr[2][0].getToken();

        if ((row1.equals("XXX"))||(row2.equals("XXX"))||(row3.equals("XXX"))||(col1.equals("XXX"))||(col2.equals("XXX"))||(col3.equals("XXX"))||(diag1.equals("XXX"))||(diag2.equals("XXX"))) {
            winner = "X";
        } else if ((row1.equals("OOO"))||(row2.equals("OOO"))||(row3.equals("OOO"))||(col1.equals("OOO"))||(col2.equals("OOO"))||(col3.equals("OOO"))||(diag1.equals("OOO"))||(diag2.equals("OOO"))) {
            winner = "O";
        } else if (cellsAvailable == 0) {
            winner = "draw";
        }

        return null;
    }

    public void drawBoard() {
        for (int row = 0; row < ROWSIZE; ++row) {
            for (int col = 0; col < COLSIZE; ++col) {
                if (cellArr[row][col].getToken() == null) {
                    System.out.print("   ");
                } else {
                    System.out.print(" " + cellArr[row][col].getToken() + " ");
                }
                if ((COLSIZE - 1) != col) {
                    System.out.print("|");
                }
            }
            if ((ROWSIZE - 1) != row) {
                System.out.print("\n--------------\n");
            }
        }
        System.out.print("\n\n");
    }

}
