import java.util.Random;
import java.util.Scanner;

public class TicTacToeTester {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int menuInput;

        do {
            TicTacToeTester.printMenu();
            menuInput = in.nextInt();

            switch (menuInput) {
                case 1:
                    TicTacToeTester.playGame();
                    break;
                case 2:
                    System.out.print("Good Bye \n");
                    break;
                default:
                    System.out.print("invalid input, try again.\n");
                    break;
            }
        } while (menuInput != 2);
    }

    public static void printMenu() {
        System.out.print("TicTacToe\n");
        System.out.print("1. Start a game\n");
        System.out.print("2. Quit\n");
    }

    public static void playGame() {
        int row;
        int col;
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        Board ticTacToeBoard = new Board();

        System.out.print("Player 1 is X's\n");
        System.out.print("Computer is O's\n");
        String curPlayer = "X";

        while (ticTacToeBoard.getWinner() == null) {
            ticTacToeBoard.drawBoard();
            if (curPlayer == "X") {
                do {
                    row = in.nextInt();
                    col = in.nextInt();
                } while (ticTacToeBoard.isCellAvailable(row, col) == false);
                ticTacToeBoard.setBoardCell(row, col, curPlayer);
                curPlayer = "O";
            } else {
                do {
                    row = rand.nextInt((2 - 0) + 1) + 0;
                    col = rand.nextInt((2 - 0) + 1) + 0;
                } while (ticTacToeBoard.isCellAvailable(row, col) == false);
                ticTacToeBoard.setBoardCell(row, col, curPlayer);
                curPlayer = "X";
            }
       }

        if (ticTacToeBoard.getWinner().equals("X")) {
            System.out.print("PLayer 1 wins!\n");
        } else if (ticTacToeBoard.getWinner().equals("O")) {
            System.out.print("Computer Wins!\n");
        } else if (ticTacToeBoard.getWinner().equals("draw")) {
            System.out.print("Draw!\n");
        }
        ticTacToeBoard.drawBoard();
    }
}
