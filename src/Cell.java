public class Cell {
    private String token;

    public Cell() {
        this.token = null;
    }

    public Cell(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
